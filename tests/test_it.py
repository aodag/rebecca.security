import pytest
from pyramid import testing
import pyramid.request


@pytest.fixture
def config():
    config = testing.setUp()
    yield config
    testing.tearDown()


class DummyAuthenticator(object):
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def authenticate(self, username, password):
        return username == password


def test_it(config):
    config.include("rebecca.security")
    config.add_authenticator(DummyAuthenticator)
    introspector = config.registry.introspector
    intr = introspector.get('rebecca', 'rebecca.security.authenticator')
    assert intr
    assert intr.title == "authenticator"
    assert intr.type_name == "test_it.DummyAuthenticator"

    request = testing.DummyRequest()
    pyramid.request.apply_request_extensions(request)
    assert request.authenticate("dummy", "dummy")
