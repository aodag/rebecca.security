from .api import (
    get_authenticator,
    authenticate,
)


def includeme(config):
    config.add_request_method(authenticate)
    config.add_request_method(
        get_authenticator,
        name="authenticator",
        reify=True,
    )
