from .interfaces import IAuthenticator


def get_authenticator(request):
    context = request.context
    return request.registry.queryMultiAdapter(
        (context, request), IAuthenticator)


def authenticate(request, username, password):
    return request.authenticator.authenticate(username, password)
