from pyramid.interfaces import IRequest
from zope.interface import Interface
from .interfaces import IAuthenticator


def includeme(config):
    config.include(".request")
    config.add_directive("add_authenticator", add_authenticator)


def add_authenticator(config, factory, values={}):
    reg = config.registry
    def register():
        reg.registerAdapter(
            factory, (Interface, IRequest), IAuthenticator)

    intr = config.introspectable(
        category_name="rebecca",
        discriminator="rebecca.security.authenticator",
        title="authenticator",
        type_name=(
            factory.__module__ +
            "." +
            factory.__name__),
    )
    intr.update(values)
    config.action("rebecca.security", register, introspectables=(intr,))
